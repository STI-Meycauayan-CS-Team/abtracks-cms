﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CefSharp;
using CefSharp.WinForms;

namespace adminAbtracks
{
    public partial class Form1 : Form
    {
        public ChromiumWebBrowser chromeBrowser;
        public ChromiumWebBrowser chromeBrowser2;

        public void InitializeChromium()
        {
            CefSettings settings = new CefSettings();
            // Initialize cef with the provided settings
            Cef.Initialize(settings);
            // Create a browser component
            chromeBrowser = new ChromiumWebBrowser("http://abtrackstracker.co.nf/");
            // Add it to the form and fill it to the form window.
            panel2.Controls.Add(chromeBrowser);
           
            chromeBrowser.Dock = DockStyle.Fill;
        }

        public void InitializeChromium2()
        {
            CefSettings settings = new CefSettings();
            // Initialize cef with the provided settings
            Cef.Initialize(settings);
            // Create a browser component
            chromeBrowser2 = new ChromiumWebBrowser("http://abtrackstracker.co.nf/");
            chromeBrowser = new ChromiumWebBrowser("https://console.firebase.google.com/project/abtrackslogin/authentication/users");
            // Add it to the form and fill it to the form window.
            panel1.Controls.Add(chromeBrowser);
            panel2.Controls.Add(chromeBrowser2);
            chromeBrowser.Dock = DockStyle.Fill;
        }
        public Form1()
        {
            InitializeComponent();

            InitializeChromium2();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
